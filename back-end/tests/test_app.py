import unittest
from src.app import app
from src.routes import routes
from src.database import database_manager


class TestMain(unittest.TestCase):
    def setUp(self):
        self.app_mock = app.test_client()

    def test_home_page(self):
        response = self.app_mock.get('/')
        self.assertEqual(200, response.status_code)


if __name__ == "__main__":
    unittest.main()
