## Documentation regarding back-end

### Simple CRUD

#### All operations must return a JSON object, for example

###### Create:

```json
({
    "message": String
},
Status_Code)
```

###### Read:

```json
({
	"message": String
	"data": {
		Everything except ID
	}
},
Status_Code)
```

###### Update:

```json
({
    "message": String
},
Status_Code)
```

###### Delete:

```json
({
    "message": String
},
Status_Code)
```
