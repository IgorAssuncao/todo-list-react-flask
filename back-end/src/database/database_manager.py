import pymongo


def init_db(db_host, db_port, db_name):
    client = pymongo.MongoClient(db_host, db_port)
    if db_name not in client.list_database_names():
        db = client[db_name]


def create_client(db_host, db_port):
    client = pymongo.MongoClient(db_port, db_port)
    try:
        # The ismaster command is cheap and does not require auth.
        client.admin.command('ismaster')
        return {
            'status': True,
            'client': client
        }
    except pymongo.mongo_client.ConnectionFailure:
        client.close()
        return {
            'status': False,
            'message': 'Create client: Failed to connect to a MongoClient'
        }


def select_db(client, database_name):
    try:
        if database_name not in client.list_database_names():
            return {
                'status': False,
                'message': 'Invalid database'
            }

        return {
            'status': True,
            'database': client.get_database(database_name)
        }
    except pymongo.errors:
        return {
            'status': False,
            'message': 'Failed to get database'
        }


def select_collection(client, database, collection_name):
    try:
        if collection_name not in database.list_collection_names():
            return {
                'status': False,
                'message': 'Invalid collection.',
            }

        return {
            'status': True,
            'collection': database.get_collection(collection_name),
        }
    except pymongo.errors.CollectionInvalid:
        return {
            'status': False,
            'message': 'Invalid Collection.',
        }


def insert_one(collection, obj):
    try:
        collection.insert_one(obj)
        return {
            'status': True,
            'message': 'Document Created.'
            }
    except pymongo.errors:
        return {
            'status': False,
            'message': 'Document Not Created.'
        }


def find_one(collection, obj):
    try:
        result = collection.find_one(obj)
        return {
            'status': True,
            'message': 'Document found.',
            'data': result
        }
    except pymongo.errors:
        return {
            'status': False,
            'message': 'Document not found.'
        }
