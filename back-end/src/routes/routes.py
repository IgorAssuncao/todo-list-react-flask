import os
from flask import request

import markdown
from database import database_manager


def hello_world(app):
    if request.method == 'GET':
        with open(os.path.dirname(app.root_path) + '/README.md', 'r') \
                as readme:
            content = readme.read()
            return markdown.markdown(content)


def insert(request, collection):
    if request.method == 'POST':
        try:
            client = database_manager.create_client()['client']
            database = database_manager.select_db(client, 'db')['database']
            collection = database_manager.select_collection(
                client, database, collection)['collection']
            result = database_manager.insert_one(collection, request.json)
            return {
                'status': result['status'],
                'message': result['message'],
                'data': str(request.json)
            }, 201
        finally:
            client.close()


def find_one(request, collection):
    if request.method == 'GET':
        try:
            client = database_manager.create_client()['client']
            database = database_manager.select_db(client, 'db')['database']
            collection = database_manager.select_collection(
                client, database, collection)['collection']
            result = database_manager.find_one(collection, request.json)
            return {
                'status': result['status'],
                'message': result['message'],
                'data': str(request.json)
            }, 200
        finally:
            client.close()
