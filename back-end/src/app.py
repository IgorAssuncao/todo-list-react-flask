from flask import Flask, request

from database import database_manager
from routes import routes


app = Flask(__name__)


@app.route('/api', methods=['GET'])
def hello_world():
    return routes.hello_world(app)


@app.route('/api/tasks/insert', methods=['POST'])
def insert():
    return routes.insert(request, 'tasks')


@app.route('/api/tasks/find_one', methods=['GET'])
def find_one():
    return routes.find_one(request, 'tasks')


if __name__ == '__main__':
    database_manager.init_db()
    app.run()
